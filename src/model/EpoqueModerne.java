package model;

import java.util.ArrayList;

public class EpoqueModerne extends Epoque{
	protected int coeffTir = 1;

	public EpoqueModerne(){
		this.nom = "Epoque moderne";
	}
	
	public Bateau prepareBateau(int taille) {
		Bateau boat = new Bateau();
		boat.setTaille(taille);
		boat.setPuissanceTir(taille*coeffTir);
		switch(taille){
		case 2:boat.setNom("Patrouilleur");
			break;
		case 3:boat.setNom("Corvette");
			break;
		case 4:boat.setNom("Croiseur");
			break;
		case 5:boat.setNom("Rainbow Warrior");
			break;
		}
		return boat;
	}
	

}
