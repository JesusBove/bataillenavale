package model;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Epoque implements Serializable {
	protected String nom;
	
	public Epoque(){
		
	}

	public String getNom() {
		return this.nom;
	}
	
	public abstract Bateau prepareBateau(int taille);
	
	public ArrayList<Bateau> prepareBateaux(int tailleGrille) {
		ArrayList<Bateau> alb = new ArrayList<Bateau>();
		
		switch(tailleGrille){
		case(5) :
					alb.add(this.prepareBateau(4));
			break;
		case(10) :
			for(int i = 2; i <= 5; i++){
				if(i == 3){
					alb.add(this.prepareBateau(3));
					alb.add(this.prepareBateau(3));
				}else{
					alb.add(this.prepareBateau(i));
				}
			}
			break;
		case(15) : 
			for(int i = 2; i <= 5; i++){
				if(i == 2){
					alb.add(this.prepareBateau(2));
					alb.add(this.prepareBateau(2));
				}
				if(i == 3){
					alb.add(this.prepareBateau(3));
					alb.add(this.prepareBateau(3));
					alb.add(this.prepareBateau(3));
					alb.add(this.prepareBateau(3));
				}
				if(i == 4){
					alb.add(this.prepareBateau(4));
					alb.add(this.prepareBateau(4));
					alb.add(this.prepareBateau(4));
				}
				if(i == 5){
					alb.add(this.prepareBateau(5));
					alb.add(this.prepareBateau(5));
				}
			}
			break;
		}
			
		return alb;
	}
}
