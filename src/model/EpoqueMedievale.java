package model;

import java.util.ArrayList;

public class EpoqueMedievale extends Epoque{
	protected float coeffTir = 0.5f;
	
	public EpoqueMedievale(){
		this.nom = "Epoque medievale";
	}

	public Bateau prepareBateau(int taille) {
		Bateau boat = new Bateau();
		boat.setTaille(taille);
		boat.setPuissanceTir((int)(taille*coeffTir));
		switch(taille){
		case 2:boat.setNom("Pirogue");
			break;
		case 3:boat.setNom("Drakkar");
			break;
		case 4:boat.setNom("Caravelle");
			break;
		case 5:boat.setNom("Galion");
			break;
		}
		return boat;
	}
}