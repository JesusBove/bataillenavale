package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import model.Bateau.Etat;
import view.BateauPlacement;

public class Partie extends Observable implements Serializable{
	protected Joueur joueur;
	protected IA ia;
	protected Epoque epoque;
	protected int tailleGrille;
	protected boolean etat;
	public enum Direction {
		NONE,
		UP,
		DOWN, 
		LEFT,
		RIGHT
	}
	private Direction lastDir = Direction.NONE; 
	private int lasti, lastj, firsti, firstj ; 
	private boolean  lastShot = false ; 
	private boolean stratCoule = false ;
	private boolean finished = false;  
	private boolean victoire = false ; 
	public Partie(){
		
	}
	
	public void nouvellePartie(){
		this.joueur = new Joueur("");
		this.ia = new IA("");
	}
	
	public void initGrilles(int taille){
		this.joueur.initGrille(taille);
		this.ia.initGrille(taille);
	}
	
	public void nouvellePartie(Joueur j, IA ia, Epoque p, int i){
		this.joueur = j;
		this.joueur.initGrille(i);
		this.ia = ia;
		this.ia.initGrille(i);
		this.epoque = p;
		this.tailleGrille = i;
		
		try {
			this.initBateau();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void initBateau(){
		this.joueur.setListeBateau(new ArrayList<Bateau>());
		this.ia.setListeBateau(new ArrayList<Bateau>());
		this.joueur.setGrille(new Bateau[this.tailleGrille][this.tailleGrille]);
		this.ia.setGrille(new Bateau[this.tailleGrille][this.tailleGrille]);
		joueur.setListeBateau(this.epoque.prepareBateaux(this.tailleGrille));
		ia.setListeBateau(this.epoque.prepareBateaux(this.tailleGrille));
		try {
			this.ia.initPlacementBateau();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setChanged();
		this.notifyObservers();
		
	}

	public void setTailleGrille(int tailleGrille) {
		this.tailleGrille = tailleGrille;
		this.joueur.initGrille(tailleGrille);
		this.ia.initGrille(tailleGrille);
		this.setChanged();
		this.notifyObservers();
	}
	
	public void setTouche(int i, int j) {
		if(this.ia.getBateau(i, j) != null) {
			this.ia.getBateau(i, j).toucher(i, j);
			this.joueur.addScore(1);
			this.joueur.incTouches();
			if(this.ia.getBateau(i, j).getEtat() == Etat.COULE){
				this.joueur.addScore(this.ia.getBateau(i, j).getTaille());
				this.joueur.incCoules();
				// maj de la portee du bot 
				// test si un bateau ennemi est dans sa portee possible si non, l'ordinateur a perdu
				this.initReachable(this.ia);
			}
		}
		
		else {
			this.joueur.incLoupe();
		}
		this.joueur.touches[i][j] = true;
		this.setChanged();
		this.notifyObservers();
	}

	public void validerPosition(Bateau b, int iDeb, int jDeb, int iFin, int jFin) throws Exception {
		b.setProprietaire(this.joueur);
		b.addElements(iDeb, jDeb, iFin, jFin);		
		this.setChanged();
		this.notifyObservers();
	}

	
	//////////////////////// Strategie de l'ordinateur ///////////////////////
	public void tourIA() {
		if(!this.isFinished()){
			switch(this.ia.getStrat()) {
			case "ALEATOIRE" : 
				this.stratAlea();
				break ; 
			case "CROIX" :
				this.stratCroix(); 
				break ; 
			}
		}	
	}
	
	public void initReachable(Participant p) {
		ArrayList<Integer> list = this.getCaseAPortee(p);
		if (p instanceof IA) {
			this.getIa().initReachable(); 
			for(int i = 0; i < list.size(); i += 2){
				int l = list.get(i);
				int c = list.get(i+1);
				this.ia.setReachableTrue(l, c);	
			}
		}
	}

	
	public void setToucheCroix( int i, int j ) {
		if(this.joueur.getBateau(i, j) != null) {
			this.joueur.getBateau(i, j).toucher(i, j);
			this.lastShot = true ;
			this.lasti = i ; 
			this.lastj = j ; 
			this.ia.addScore(1);
			this.ia.incTouches();
			if (this.joueur.getBateau(i, j).getEtat() == Etat.COULE) {
				this.ia.addScore(this.joueur.getBateau(i, j).getTaille());
				this.ia.incCoules();
				this.stratCoule = false ; 
				this.lastDir = Direction.NONE ; 
				// maj de la portee du joueur
				// test si un bateau ennemi est dans sa portee possible si non, le joueur a perdu
			}
		}
		else {
			this.ia.incLoupe(); 
			this.lastShot=false ; 
		}	
		this.ia.touches[i][j] = true;
		this.setChanged();
		this.notifyObservers();
	}
	public void stratAlea() {
		if (!victoire) {
			ArrayList<Integer> ali = this.getCaseAPortee(this.ia) ; 
			int index = (int)(Math.random() * (ali.size()-1)) + 0;
			while (index%2 != 0) index = (int)(Math.random() * (ali.size()-1)) + 0;
			int i=ali.get(index) ; 
			int j=ali.get(index+1) ;
			while ( this.ia.touches[i][j]==true ) {
				index = (int)(Math.random() * (ali.size()-1)) + 0;
				while (index%2 != 0) index = (int)(Math.random() * (ali.size()-1)) + 0;
				i=ali.get(index) ; 
				j=ali.get(index+1) ;
			}
			if(this.joueur.getBateau(i, j) != null) {
				this.joueur.getBateau(i, j).toucher(i, j);
				this.ia.addScore(1);
				this.ia.incTouches();
				if(this.joueur.getBateau(i, j).getEtat() == Etat.COULE){
					this.ia.addScore(this.joueur.getBateau(i, j).getTaille());
					this.ia.incCoules();
					// maj de la portee du joueur
					// test si un bateau ennemi est dans sa portee possible si non, le joueur a perdu
				}
				
				this.lastShot = true ;
				this.stratCoule = true ; 
				this.lasti = i ; 
				this.lastj = j ; 
				this.firsti = i ; 
				this.firstj = j ; 
			}
			else {
				this.ia.incLoupe(); 
			}	
			this.ia.touches[i][j] = true;
			if(!this.finished ){
				this.setChanged();
				this.notifyObservers();
			}
		}
	}
	
	public void stratCroix() {
		if ( !this.stratCoule ) {
			this.stratAlea(); 
		} else {
			boolean condUp = firsti-1>=0 && this.ia.touches[firsti-1][firstj]!=true && this.ia.reachable[firsti-1][firstj]==true;
			boolean condLeft = firstj-1>=0 && this.ia.touches[firsti][firstj-1]!=true && this.ia.reachable[firsti][firstj-1]==true ; 
			boolean condRight = firstj+1<this.tailleGrille && this.ia.touches[firsti][firstj+1]!=true && this.ia.reachable[firsti][firstj+1]==true ; 
			boolean condDown = firsti+1<this.tailleGrille && this.ia.touches[firsti+1][firstj]!=true && this.ia.reachable[firsti+1][firstj]==true ; 
			switch ( lastDir ){
			case NONE : 
				if (condLeft) {
					lastDir = Direction.LEFT ; 
					this.setToucheCroix(firsti, firstj-1);
				}
				else if (condRight) {
					lastDir = Direction.RIGHT ; 
					this.setToucheCroix(firsti, firstj+1);
				}
				else if (condUp) {
					lastDir= Direction.UP ; 
					this.setToucheCroix(firsti-1, firstj);
				}
				else if (condDown) {
					lastDir = Direction.DOWN ; 
					this.setToucheCroix(firsti+1, firstj);
				}
				break ; 
			case LEFT : 
				if (lastShot && lastj-1>=0 && this.ia.touches[lasti][lastj-1]!=true && this.ia.reachable[lasti][lastj-1]==true) {
					this.setToucheCroix(lasti, lastj-1);
				} else {
					if (condRight) {
						lastDir = Direction.RIGHT ; 
						this.setToucheCroix(firsti, firstj+1);
					}
					else if (condDown) {
						lastDir = Direction.DOWN ; 
						this.setToucheCroix(firsti+1, firstj);
					}
					else if (condUp) {
						lastDir= Direction.UP ; 
						this.setToucheCroix(firsti-1, firstj);
					}
					else {
						this.lastShot=false ;
						this.stratCoule=false ; 
						lastDir = Direction.NONE ; 
						this.stratAlea(); 
					}
				}
				break ; 
			case RIGHT :
				if (lastShot && lastj+1<this.tailleGrille && this.ia.touches[lasti][lastj+1]!=true && this.ia.reachable[lasti][lastj+1]==true) {
					this.setToucheCroix(lasti, lastj+1);
				} else {
					if (condLeft) {
						lastDir = Direction.LEFT ; 
						this.setToucheCroix(firsti, firstj-1);
					}
					else if (condDown) {
						lastDir = Direction.DOWN ; 
						this.setToucheCroix(firsti+1, firstj);
					}
					else if (condUp) {
						lastDir= Direction.UP ; 
						this.setToucheCroix(firsti-1, firstj);
					}
					else {
						this.lastShot=false ;
						this.stratCoule=false ; 
						lastDir = Direction.NONE ; 
						this.stratAlea(); 
					}
					
				}
				break ; 
			case UP : 
				if (lastShot && lasti-1>=0 && this.ia.touches[lasti-1][lastj]!=true && this.ia.reachable[lasti-1][lastj]==true) {
					this.setToucheCroix(lasti-1,lastj);
				} else {
					if (condDown) {
						lastDir= Direction.DOWN ; 
						this.setToucheCroix(firsti+1, firstj);
					}	
					else if (condLeft) {
						lastDir = Direction.LEFT ; 
						this.setToucheCroix(firsti, firstj-1);
					}	
					else if (condRight) {
						lastDir = Direction.RIGHT ; 
						this.setToucheCroix(firsti, firstj+1);
					}
					else {
						this.lastShot=false ;
						this.stratCoule=false ; 
						lastDir = Direction.NONE ; 
						this.stratAlea(); 
					}
						
				}
				break ; 
			case DOWN : 
				if (lastShot && lasti+1<this.tailleGrille && this.ia.touches[lasti+1][lastj]!=true && this.ia.reachable[lasti+1][lastj]==true) {
					this.setToucheCroix(lasti+1, lastj);
				} else {
					if (condUp) {
						lastDir = Direction.UP ; 
						this.setToucheCroix(firsti-1, firstj);
					}
					else if (condLeft) {
						lastDir = Direction.LEFT ; 
						this.setToucheCroix(firsti, firstj-1);
					}
					else if (condRight) {
						lastDir = Direction.RIGHT ; 
						this.setToucheCroix(firsti, firstj+1);
					}
					else {
						this.lastShot=false ;
						this.stratCoule=false ; 
						lastDir = Direction.NONE ; 
						this.stratAlea(); 
					}

				}
				break ; 
			}
			
				
		}
			
	}
	
	public ArrayList<Integer> getBateauAPortee(Participant p) {
		ArrayList<Integer> ali = new ArrayList<Integer>();
		for(Bateau b : p.getListeBateau()){
			if(b.etat != Etat.COULE){
				int i1 = b.lelem.get(0).getX();
				int j1 = b.lelem.get(0).getY();
				int i2 = b.lelem.get(b.lelem.size()-1).getX();
				int j2 = b.lelem.get(b.lelem.size()-1).getY();
				
				for(int i = i1 ; i <= i2 ; i++){
					for(int j = j1 ; j <= j2 ; j++){
						if(i >= 0 && j >= 0 && i < this.tailleGrille && j < this.tailleGrille){
							ali.add(i);
							ali.add(j);
						}
					}
				} 
			}
		}
		return ali ; 
	}
	
	public ArrayList<Integer> getCaseAPortee(Participant p){
		ArrayList<Integer> ali = new ArrayList<Integer>();
		
		for(Bateau b : p.getListeBateau()){
			if(b.etat != Etat.COULE){
				int i1 = b.lelem.get(0).getX() - b.puissanceTir;
				int j1 = b.lelem.get(0).getY() - b.puissanceTir;
				int i2 = b.lelem.get(b.lelem.size()-1).getX() + b.puissanceTir;
				int j2 = b.lelem.get(b.lelem.size()-1).getY() + b.puissanceTir;
				
				for(int i = i1 ; i <= i2 ; i++){
					for(int j = j1 ; j <= j2 ; j++){
						if(i >= 0 && j >= 0 && i < this.tailleGrille && j < this.tailleGrille){
							ali.add(i);
							ali.add(j);
						}
					}
				}

			}
		}
		return ali;	
	}
	
	public void update() {
		this.setChanged();
		this.notifyObservers();		
	}

	public boolean isFinished() {
		if(this.getJoueur().flotteCoulee() | this.getIa().flotteCoulee()){		
			this.finished = true;
			return true;
		}
		
		if(this.impossibleToucher(this.joueur)){
			if(!victoire) {
				this.finished = true ; 
				this.ia.addScore(30);
				this.victoire = true;
				return true;
			}
		}
		else if(this.impossibleToucher(this.ia)){
			this.victoire = true;
			this.finished = true; 
			return true;
		}

		
		return false;
	}
	
	public boolean impossibleToucher(Joueur j){
		ArrayList<Integer> list = this.getCaseAPortee(j);
		
		for(int i = 0; i < list.size(); i+=2){
			int x = list.get(i);
			int y = list.get(i+1);
	
			
			for(Bateau b : this.ia.getListeBateau()){
				for(Element e : b.getLelem()){
					if(e.x == x && e.y == y && !e.touche){					
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	public boolean impossibleToucher(IA ia){
		ArrayList<Integer> list = this.getCaseAPortee(ia);
		for(int i = 0; i < list.size()/2; i++){
			int x = list.get(i);
			int y = list.get(i+1);
			
			for(Bateau b : this.joueur.getListeBateau()){
				for(Element e : b.getLelem()){
					if(e.x == x && e.y == y && !e.touche){
						return false;
					}
				}
			}
		}
		
		return true;
	}

	
	public Participant getGagnant(){
		if(this.getJoueur().getScore() > this.getIa().getScore()){
			return this.getJoueur();
		}else{
			return this.getIa();
		}
	}

	public Direction getLastDir() {
		return lastDir;
	}

	public void setLastDir(Direction lastDir) {
		this.lastDir = lastDir;
	}

	public int getLasti() {
		return lasti;
	}

	public void setLasti(int lasti) {
		this.lasti = lasti;
	}

	public int getLastj() {
		return lastj;
	}

	public void setLastj(int lastj) {
		this.lastj = lastj;
	}

	public int getFirsti() {
		return firsti;
	}

	public void setFirsti(int firsti) {
		this.firsti = firsti;
	}

	public int getFirstj() {
		return firstj;
	}

	public void setFirstj(int firstj) {
		this.firstj = firstj;
	}

	public boolean isLastShot() {
		return lastShot;
	}

	public void setLastShot(boolean lastShot) {
		this.lastShot = lastShot;
	}

	public boolean isStratCoule() {
		return stratCoule;
	}

	public void setStratCoule(boolean stratCoule) {
		this.stratCoule = stratCoule;
	}
	
	
	
	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	public IA getIa() {
		return ia;
	}

	public void setIa(IA ia) {
		this.ia = ia;
	}

	public Epoque getEpoque() {
		return epoque;
	}

	public void setEpoque(Epoque epoque) {
		this.epoque = epoque;
		setChanged();
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public int getTailleGrille() {
		return tailleGrille;
	}

	
}
