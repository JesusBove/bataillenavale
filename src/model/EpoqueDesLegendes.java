package model;

import java.util.ArrayList;

public class EpoqueDesLegendes extends Epoque{
	protected int coeffTir = 10;
	
	public EpoqueDesLegendes(){
		this.nom = "Epoque des legendes";
	}

	public Bateau prepareBateau(int taille) {
		Bateau boat = new Bateau();
		boat.setTaille(taille);
		boat.setPuissanceTir(taille*coeffTir);
		switch(taille){
		case 2:boat.setNom("Kon-Tiki");
			break;
		case 3:boat.setNom("The Flying Dutchman");
			break;
		case 4:boat.setNom("Queen Anne's revenge");
			break;
		case 5:boat.setNom("Noah's ark");
			break;
		}
		return boat;
	}
}