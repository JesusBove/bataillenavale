package model;

import java.io.Serializable;

public class Element implements Serializable{
	public int x;
	public int y;
	protected boolean touche = false;

	public Element(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isTouche(){
		return this.touche;
	}
	
	public void touche(){
		this.touche = true;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}

}
