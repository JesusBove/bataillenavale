package model;

import java.io.Serializable;
import java.util.ArrayList;

import model.Bateau.Etat;

public abstract class Participant implements Serializable{
	protected String nom;
	protected int nbTouche = 0;
	protected int nbLoupe = 0;
	protected Bateau grille[][];
	protected boolean touches[][];
	protected ArrayList<Bateau> listeBateau = new ArrayList<Bateau>();
	protected int score = 0;
	protected int nbCoules = 0;
	protected boolean victoire = false ; 
	
	

	public Participant(String nom){
		this.nom = nom;
	}
	
	public void initGrille(int nb){
		this.grille = new Bateau[nb][nb];
		this.touches = new boolean[nb][nb];
		
		for(boolean[] b : touches){
			for(boolean bo : b){
				bo = false;
			}
		}
	}
	
	public boolean[][] getTouches(){
		return this.touches;
	}
	
	public boolean getTouche(int i, int j) {
		return touches[i][j] ; 
	}
	
	public void setTouches(int i, int j) {
		this.touches[i][j]=true ; 
	}
	
	public void placerBateau(Bateau b, int x, int y){
		this.grille[x][y] = b; 
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbTouche() {
		return nbTouche;
	}

	public void setNbTouche(int nbTouche) {
		this.nbTouche = nbTouche;
	}
	
	public void incLoupe() {
		this.nbLoupe++;
	}

	public int getNbLoupe() {
		return nbLoupe;
	}

	public void setNbLoupe(int nbLoupe) {
		this.nbLoupe = nbLoupe;
	}

	public Bateau[][] getGrille() {
		return grille;
	}
	
	public Bateau getBateau(int i, int j) {
		return this.grille[i][j];
	}

	public void setGrille(Bateau[][] grille) {
		this.grille = grille;
	}

	public ArrayList<Bateau> getListeBateau() {
		return listeBateau;
	}

	public void setListeBateau(ArrayList<Bateau> listeBateau) {
		this.listeBateau = listeBateau;
	}

	public void addBateau(Bateau bateau) {
		this.listeBateau.add(bateau);		
	}
	
	public void addScore(int i) {
		this.score += i;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public boolean flotteCoulee(){
		for(Bateau b : this.listeBateau){
			if(b.etat != Etat.COULE){
				return false;
			}
		}
		
		return true;
	}

	public int getNbCoules(){
		return this.nbCoules;
	}
	
	public void incCoules(){
		this.nbCoules++;
	}
	
	public void incTouches(){
		this.nbTouche++;
	}
	
	public boolean isVictoire() {
		return victoire;
	}

	public void setVictoire(boolean victoire) {
		this.victoire = victoire;
	}

	public void setTouches(boolean[][] touches) {
		this.touches = touches;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setNbCoules(int nbCoules) {
		this.nbCoules = nbCoules;
	}
}
