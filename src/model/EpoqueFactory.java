package model;

public class EpoqueFactory {
	private static EpoqueFactory epf= null;

	public static EpoqueFactory getInstance() {
		if(epf == null) {
			return new EpoqueFactory();
		}
		return epf;
	}
	
	public Epoque nouvelleEpoque(String type) {
		if(type.equals("Medievale")) {
			return new EpoqueMedievale();
		}
		if(type.equals("Futuriste")) {
			return new EpoqueFuturiste();
		}
		if(type.equals("Moderne")) {
			return new EpoqueModerne();
		}
		if(type.equals("Legende")) {
			return new EpoqueDesLegendes();
		}
		return new EpoqueModerne();
	}
	
}
