package model;

import java.util.ArrayList;

import view.ImageFactory;

public class IA extends Participant{
	
	private enum Strategie{
		ALEATOIRE,
		CROIX
	}
	
	private Strategie strat ; 
	protected boolean[][] reachable ;  
	
	

	public IA(String nom) {
		super(nom);
	}
	
	public void initReachable() {
		reachable = new boolean[this.grille.length][this.grille.length] ;
		for ( int i=0 ; i<grille.length ; i++ ) {
			for ( int j=0 ; j<grille.length ; j++ ) {
				reachable[i][j]=false ; 
			}
		}
	}
	
	public void setReachableTrue(int i, int j) {
		this.reachable[i][j]=true ; 
	}
	
	public void initPlacementBateau() throws Exception {
		int taille = this.grille.length;
		for(Bateau b: this.listeBateau) {
			b.setIA(this);
			boolean test = true;
			while(test) {
				int randX = (int) (Math.random()*taille);
				int randY = (int) (Math.random()*taille);
				if(this.grille[randX][randY] == null) {
					int randOrientation = (int) (Math.random()*4);
					boolean orient = true;
					switch(randOrientation) {
						case 0: for(int i=0; i<b.getTaille(); i++) {
									if(randX+i>=taille || this.grille[randX+i][randY] != null) {
										orient = false;
									}
								}
								if(orient) {
									for(int i=0; i<b.getTaille(); i++) {
										this.grille[randX+i][randY] = b;
									}
									test = false;
									b.addElements(randX, randY, randX+b.getTaille()-1, randY);
								}
							break;
						case 1: for(int i=0; i<b.getTaille(); i++) {
									if(randX-i<0 || this.grille[randX-i][randY] != null) {
										orient = false;
									}
								}
								if(orient) {
									for(int i=0; i<b.getTaille(); i++) {
										this.grille[randX-i][randY] = b;
									}
									test = false;
									b.addElements(randX, randY, randX-(b.getTaille()-1), randY);
								}
							break;
						case 2: for(int i=0; i<b.getTaille(); i++) {
									if(randY+i>=taille || this.grille[randX][randY+i] != null) {
										orient = false;
									}
								}
								if(orient) {
									for(int i=0; i<b.getTaille(); i++) {
										this.grille[randX][randY+i] = b;
									}
									test = false;
									b.addElements(randX, randY, randX, randY+b.getTaille()-1);
								}
							break;
						case 3: for(int i=0; i<b.getTaille(); i++) {
									if(randY-i<0 || this.grille[randX][randY-i] != null) {
										orient = false;
									}
								}
								if(orient) {
									for(int i=0; i<b.getTaille(); i++) {
										this.grille[randX][randY-i] = b;
									}
									test = false;
									b.addElements(randX, randY, randX, randY-(b.getTaille()-1));
								}
							break;
					}
				}
			}
		}
	}

	public String getStrat() {
		return strat.toString();
	}

	public void setStrat(String strat) {
		switch (strat ) {
		case "Aleatoire" :
			this.strat = Strategie.ALEATOIRE ; break ; 
		case "Croix" : 
			this.strat = Strategie.CROIX ; break ; 
		}
		
	}
	

}
