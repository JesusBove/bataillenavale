package model;

import java.util.ArrayList;

public class EpoqueFuturiste extends Epoque{
	protected int coeffTir = 2;
	
	public EpoqueFuturiste(){
		this.nom = "Epoque futuriste";
	}

	public Bateau prepareBateau(int taille) {
		Bateau boat = new Bateau();
		boat.setTaille(taille);
		boat.setPuissanceTir(taille*coeffTir);
		switch(taille){
		case 2:boat.setNom("DestroyerI");
			break;
		case 3:boat.setNom("DestroyerII");
			break;
		case 4:boat.setNom("DestroyerIII");
			break;
		case 5:boat.setNom("DestroyerIV");
			break;
		}
		return boat;
	}
}