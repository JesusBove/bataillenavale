package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Bateau implements Serializable{
	public String nom;
	public Participant proprietaire;
	public int taille;
	public int puissanceTir;
	public int compteurTouche = 0;
	public ArrayList<Element> lelem = new ArrayList<Element>();
	public Etat etat = Etat.INDEMNE;
	
	public enum Etat {
		  TOUCHE,
		  COULE,
		  INDEMNE 
		}
	
	public Bateau(){
		
	}
		
	public Bateau(String nom, int taille, int puissanceTir, int x , int y, int x2, int y2){
		this.nom = nom;
		this.taille = taille;
		this.puissanceTir = puissanceTir;
		
		if(x == x2){
			if(y<y2) {
				for(int i = y; i <= y2; i++){
					lelem.add(new Element(x,i));
				}
			}
			else {
				for(int i = y2; i <= y; i++){
					lelem.add(new Element(x,i));
				}
			}
		}else if(y == y2){
			if(x<x2) {
				for(int i = x; i <= x2; i++){
					lelem.add(new Element(i,y));
				}
			}
			else {
				for(int i = x2; i <= x; i++){
					lelem.add(new Element(i,y));
				}
			}
		}
	}
	
	public void addElements(int x1, int y1, int x2, int y2) throws Exception{
		int xd,xf,yd,yf;
		
		xd = Math.min(x1, x2);
		xf = Math.max(x1, x2);
		yd = Math.min(y1, y2);
		yf = Math.max(y1, y2);
		
		if(x1 != x2){
			if(xf - xd != taille -1){
				throw new Exception("Taille invalide");
			}
			
			for(int i = xd; i <= xf; i++){
				this.lelem.add(new Element(i,y1));
				this.proprietaire.grille[i][y1] = this;
			}
		}else{
			if(yf - yd != taille -1){
				throw new Exception("Taille invalide | Taille attendue : " + this.taille + " | Debut du bateau : " + xd + " " + yd + " | Fin : " + xf + " " + yf );
			}
			for(int i = yd; i <= yf; i++){
				this.lelem.add(new Element(x1,i));
				this.proprietaire.grille[x1][i] = this;
			}
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public Participant getProprietaire() {
		return proprietaire;
	}



	public void setProprietaire(Participant proprietaire) {
		this.proprietaire = proprietaire;
	}

	public void setIA(IA ia) {
		this.proprietaire = ia;
	}

	public int getPuissanceTir() {
		return puissanceTir;
	}

	public void setPuissanceTir(int puissanceTir) {
		this.puissanceTir = puissanceTir;
	}

	public int getCompteurTouche() {
		return compteurTouche;
	}

	public void setCompteurTouche(int compteurTouche) {
		this.compteurTouche = compteurTouche;
	}

	public ArrayList<Element> getLelem() {
		return lelem;
	}

	public void setLelem(ArrayList<Element> lelem) {
		this.lelem = lelem;
	}

	public Element getElement(int i, int j) {
		for(Element e : this.lelem){
			if(e.x == i && e.y == j){
				return e;
			}
		}
		
		return null;
	}

	public void toucher(int i, int j) {
		if(!this.getElement(i, j).isTouche()){
			this.getElement(i,j).touche();
			this.compteurTouche++;	
		}
	
		if(this.etat == Etat.INDEMNE){
			this.etat = Etat.TOUCHE;
		}else if(this.etat == Etat.TOUCHE && this.compteurTouche == taille){
			this.etat = Etat.COULE;
		}
	}
	
	public Etat getEtat() {
		return this.etat; 
	}
	
	
}
