package model;

import java.util.ArrayList;

public class EpoqueContemporaine extends Epoque{
	protected float coeffTir = 2;

	public EpoqueContemporaine() {
		this.nom = "Epoque Contemporaine";
	}
	
	public Bateau prepareBateau(int taille){
		Bateau boat = new Bateau();
		boat.setTaille(taille);
		boat.setPuissanceTir((int)(taille*coeffTir));
		switch(taille){
		case 2:boat.setNom("Radeau");
			break;
		case 3:boat.setNom("Kayak");
			break;
		case 4:boat.setNom("Peniche");
			break;
		case 5:boat.setNom("Deltaplane Amphibie de 500m de long");
			break;
		}
		return boat;
	}
}
