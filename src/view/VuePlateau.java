package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.*;
import model.Bateau.Etat;

public class VuePlateau extends JPanel implements Observer{
	private Case[][] grilleJoueur;
	private JPanel grilleJoueurGUI = new JPanel();
	private Case[][] grilleIA;
	private JPanel grilleIAGUI = new JPanel();
	private JPanel info = new JPanel();
	private Partie model;

	public VuePlateau(Partie model) {
		this.model = model;
		this.creation(model.getTailleGrille());		
	}

	public void creation(int taille){
		this.setLayout(new GridLayout(0,2));
		JLabel titre = new JLabel("Grilles de jeu") ;

		this.grilleJoueurGUI.setLayout(new GridLayout(20,20));
		this.grilleJoueurGUI.setSize(200,200);
		this.grilleIAGUI.setLayout(new GridLayout(20,20));

		this.grilleJoueur = new Case[taille][taille];
		this.grilleIA = new Case[taille][taille];
		for(int i=0; i<20; i++) {
			for(int j=0; j<20; j++) {
				if(i < (taille +1)  && j < (taille+1)){
					if(i==0&&j==0){ grilleJoueurGUI.add( new JLabel(" ") ) ;
					grilleIAGUI.add( new JLabel(" ") ) ; 
					}
					else if(i==0 && j>0) {grilleJoueurGUI.add( new JLabel( (char)(j+64) + " ") ) ; 
					grilleIAGUI.add( new JLabel( (char)(j+64) + " ") ) ;}

					else if(j==0) {grilleJoueurGUI.add( new JLabel( ( i-1 + " ") ) ) ;
					grilleIAGUI.add( new JLabel( ( i-1 + " ") ) ) ;}
					else {
						this.grilleJoueur[i-1][j-1] = new Case(i-1,j-1);
						this.grilleJoueur[i-1][j-1].setEnabled(false);
						this.grilleJoueur[i-1][j-1].setDisabledIcon(this.grilleJoueur[i-1][j-1].getIcon());
						grilleJoueurGUI.add(this.grilleJoueur[i-1][j-1]);
						this.grilleIA[i-1][j-1] = new Case(i-1,j-1);
						this.grilleIA[i-1][j-1].setEnabled(false);
						this.grilleIA[i-1][j-1].addActionListener(new LocalBotListener(this.model,i-1,j-1));
						grilleIAGUI.add(this.grilleIA[i-1][j-1]);
					}
				}
				else{
					grilleJoueurGUI.add(new JLabel(""));
					grilleIAGUI.add(new JLabel(""));
				}
			}
		}
		this.info.setLayout(new BorderLayout());
		this.add(this.grilleJoueurGUI);

		this.add(this.grilleIAGUI);
		this.model.update();
	}

	class LocalBotListener implements ActionListener {

		private Partie model ; 
		private int i,j ; 

		public LocalBotListener(Partie model, int i, int j) {
			this.model=model ; 
			this.i=i ; 
			this.j=j ; 
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			this.model.setTouche(i,j);
			this.model.tourIA();
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		
		//Creation de la grille aux bonnes dimensions
		if (model.getTailleGrille()!=this.grilleIA.length) {
			this.removeAll();
			this.grilleIAGUI.removeAll();
			this.grilleJoueurGUI.removeAll();
			this.creation(model.getTailleGrille()); 
		}
		
		for(int i=0; i<model.getTailleGrille(); i++) {
			for(int j=0; j<model.getTailleGrille(); j++) {
				this.grilleIA[i][j].setEnabled(false);	
			}
		}

		int count = 0;
		for(Bateau b : this.model.getJoueur().getListeBateau()){
			count++;
		}

		ArrayList<Integer> ali = model.getCaseAPortee(model.getJoueur());
		
		for(int i = 0; i < ali.size(); i += 2){
			int l = ali.get(i);
			int c = ali.get(i+1);
			this.grilleIA[l][c].setEnabled(true);	
			this.grilleIA[l][c].setIcon(ImageFactory.getInstance().getMer());
		}

		for(int i=0; i<model.getTailleGrille(); i++) {
			for(int j=0; j<model.getTailleGrille(); j++) {
				
				if(this.model.getJoueur().getBateau(i, j) != null) {
					if(this.model.getJoueur().getBateau(i, j).etat == Etat.COULE) {
						this.grilleJoueur[i][j].setIcon(ImageFactory.getInstance().getCroix_noire());
					}else if(this.model.getJoueur().getBateau(i, j).getElement(i, j).isTouche()) {
						this.grilleJoueur[i][j].setIcon(ImageFactory.getInstance().getCroix_rouge());					
					}
					else {
						this.grilleJoueur[i][j].setIcon(null);
					}
				}else{
					this.grilleJoueur[i][j].setIcon(ImageFactory.getInstance().getMer());
				}
				
				if(model.getIa().getTouches()[i][j]){
					if(this.model.getJoueur().getBateau(i, j) == null){
						this.grilleJoueur[i][j].setIcon(ImageFactory.getInstance().getMer_touche());
					}
					
					this.grilleJoueur[i][j].setEnabled(false);	
					this.grilleJoueur[i][j].setDisabledIcon(this.grilleJoueur[i][j].getIcon());	
				}else{
					this.grilleJoueur[i][j].setEnabled(false);	
					this.grilleJoueur[i][j].setDisabledIcon(this.grilleJoueur[i][j].getIcon());	
				}


				//Plateau de l'IA
				if(this.model.getIa().getBateau(i, j) != null) {
					if(this.model.getIa().getBateau(i, j).etat == Etat.COULE) {
						this.grilleIA[i][j].setIcon(ImageFactory.getInstance().getCroix_noire());
					}else if(this.model.getIa().getBateau(i, j).getElement(i, j).isTouche()) {
						this.grilleIA[i][j].setIcon(ImageFactory.getInstance().getCroix_rouge());					
					}						
				}

				if(model.getJoueur().getTouches()[i][j]){
					if(this.model.getIa().getBateau(i, j) == null){
						this.grilleIA[i][j].setIcon(ImageFactory.getInstance().getMer_touche());
					}
					this.grilleIA[i][j].setEnabled(false);	
					this.grilleIA[i][j].setDisabledIcon(this.grilleIA[i][j].getIcon());	
				}
			}
		}
		

	}
}
