package view;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.Bateau;
import model.Element;
import model.EpoqueFactory;
import model.Joueur;
import model.Partie;

public class VuePlacement extends JPanel implements Observer{

	private Partie model ; 
	private JButton valider = new JButton("Valider") ;
	private Case[][] grille ;
	private JPanel grilleGUI;
	private ArrayList<BateauPlacement> lbutton ; 
	private JPanel listBoat = new JPanel();
	private JLabel titre = new JLabel("Placement des bateaux"); 
	private static final int BATEAU = -100;
	private static final int CASE = -200;
	private static final int VALIDER = -300;
	private BateauPlacement bateauChoisi;
	private Placement place;

	private enum Placement{
		DEBUT,
		FIN
	}

	public VuePlacement(Partie model) {	
		this.model=model ;
		this.creation(model.getTailleGrille());
		this.creationListe();
	}

	public Placement getPlace() {
		return place;
	}

	public void setPlace(Placement place) {
		this.place = place;
	}

	public void creation(int taille) {
		grilleGUI= new JPanel() ; 
		grilleGUI.setLayout(new GridLayout(20, 20));
		this.setLayout(new BorderLayout());
		this.grille = new Case[taille][taille];
		for(int i=0; i<20; i++) {
			for(int j=0; j<20; j++) {
				if(i < (taille +1)  && j < (taille+1)){
					if(i==0&&j==0) grilleGUI.add( new JLabel(" ") ) ; 
					else if(i==0 && j>0) grilleGUI.add( new JLabel( (char)(j+64) + "") ) ; 
					else if(j==0) grilleGUI.add( new JLabel( ( i + "") ) ) ;
					else{
						this.grille[i-1][j-1] = new Case(i-1,j-1);
						this.grille[i-1][j-1].addActionListener(new VuePlacement.LocalListener(CASE,this.grille[i-1][j-1], this));
						this.grille[i-1][j-1].setEnabled(false) ; 
						grilleGUI.add(this.grille[i-1][j-1]);

					}

				}else{
					grilleGUI.add(new JLabel(""));
				}

			}
		}
		titre.setText("Placement des Bateaux : ");
		this.add(titre, BorderLayout.NORTH);
		this.add(grilleGUI, BorderLayout.CENTER);

	}
	public void creationListe() {
		// Liste des bateaux 
		lbutton = new ArrayList<BateauPlacement>();

		for(Bateau b : this.model.getJoueur().getListeBateau()){
			lbutton.add(new BateauPlacement(b)) ; 
		}

		listBoat.setLayout(new GridLayout(lbutton.size()+1,0));
		listBoat.setPreferredSize(new Dimension(200,200));
		listBoat.add(new JLabel("Bateaux a placer"));


		int R = (int)(Math.random() * (255-0)) + 0;
		int G = (int)(Math.random() * (255-0)) + 0;
		int B = (int)(Math.random() * (255-0)) + 0;
		Color c = new Color(R,G,B);

		for ( int i=0; i<lbutton.size() ; i++ ) {
			R = (int)(Math.random() * (255-0)) + 0;
			G = (int)(Math.random() * (255-0)) + 0;
			B = (int)(Math.random() * (255-0)) + 0;
			c = new Color(R,G,B);

			lbutton.get(i).setBackground(c);
			listBoat.add(lbutton.get(i)) ; 
			lbutton.get(i).addActionListener(new VuePlacement.LocalListener(BATEAU,lbutton.get(i),this));
		}

		this.add(valider, BorderLayout.SOUTH);
		this.valider.addActionListener(new VuePlacement.LocalListener(VALIDER,this));
		this.valider.setEnabled(false);

		this.add(listBoat, BorderLayout.EAST);
	}



	public BateauPlacement getBateauChoisi() {
		return bateauChoisi;
	}

	public void setBateauChoisi(BateauPlacement bateauChoisi) {
		this.bateauChoisi = bateauChoisi;
	}



	class LocalListener implements ActionListener {
		private VuePlacement vp;
		private int digit;
		private Case cas ; 
		private BateauPlacement bat;
		private boolean bool  ; 

		public LocalListener(int digit,BateauPlacement bat, VuePlacement vp) {
			this.vp = vp;
			this.bat=bat; 
			this.digit=digit ; 
		}

		public LocalListener(int digit, VuePlacement vp) {
			this.vp = vp;
			this.digit=digit ; 
		}

		public LocalListener(int digit,Case cas,VuePlacement vp) {
			this.vp = vp;
			this.cas=cas ;
			this.digit=digit ; 
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if(digit==BATEAU) {
				valider.setEnabled(false);
				bat.reset();

				vp.setBateauChoisi(bat);
				vp.setPlace(Placement.DEBUT);

				for( BateauPlacement b : lbutton ) {
					b.setEnabled(false);
				}

				Thread t = new Thread() {
					public void run() {
						for( int i=0 ; i<grille.length ; i++ ) {
							for ( int j=0 ; j<grille.length ; j++ ){							
								if((i - bat.getB().getTaille() +1) < 0 &&
										(i + bat.getB().getTaille() -1) >= grille.length &&
										(j - bat.getB().getTaille() +1) < 0 &&
										(j + bat.getB().getTaille() -1) >= grille.length){
									grille[i][j].setEnabled(false);
									
									
								}else{
									grille[i][j].setEnabled(true);
								}
							}	
						}
					}
				};
				t.start();
				
            			for(BateauPlacement bp : vp.lbutton){
            				if(bp.getiFin() != -1){

            					int ideb = bp.getiDeb();
            					int ifin = bp.getiFin();
            					int jdeb = bp.getjDeb();
            					int jfin = bp.getjFin();

            					int i1 = Math.min(ideb, ifin);
            					int i2 = Math.max(ideb, ifin);
            					int j1 = Math.min(jdeb, jfin);
            					int j2 = Math.max(jdeb, jfin);

            					if(bp.getiFin() != bp.getiDeb()){    
            						for(int i = i1; i <= i2; i++){
            							grille[i][j1].setEnabled(false);
            						}
            					}else{
            						for(int j = j1; j <= j2; j++){
            							grille[i1][j].setEnabled(false);
            						}
            					}
            				}
            			}
			}
			else if(digit==CASE) {	
				if(vp.getBateauChoisi() != null){
					if(vp.getPlace() == Placement.DEBUT){
						Thread t = new Thread() {
							public void run() {
								vp.griser(cas.getI(),cas.getJ(),vp.getBateauChoisi().getB().getTaille());
							}
						};
						t.start();

						vp.bateauChoisi.setiDeb(cas.getI());
						vp.bateauChoisi.setjDeb(cas.getJ());
						vp.setPlace(Placement.FIN);
					}else if(vp.getPlace() == Placement.FIN){
						vp.bateauChoisi.setiFin(cas.getI());
						vp.bateauChoisi.setjFin(cas.getJ());
						Thread t = new Thread() {
							public void run() {
								vp.resetBateau();
							}
						};
						t.start();

					}
				}
			}
			else if(digit == VALIDER){
				Joueur j = model.getJoueur();
				for(BateauPlacement bp : vp.lbutton){
					try {
						if(bp.getjFin() != -1){
							bp.validerPosition(vp.model);	
						}

					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}

				JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(vp);
				((JeuGUI)topFrame).setVuePlateau();
				model.initReachable(model.getIa());
				model.initReachable(model.getJoueur());
			}
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		grilleGUI.removeAll(); 
		this.creation(model.getTailleGrille());	
		listBoat.removeAll();
		this.creationListe();

	}

	public void griser(int i, int j,int taille) {
		for(int ib = 0; ib < grille.length; ib++){
			for(int jb = 0; jb < grille.length; jb++){					
				if(ib==i && jb == j - taille +1){
					for(int jt = j-taille + 1; jt < j; jt++){
						if(this.isOccupe(i, jt)){
							grille[ib][jb].setEnabled(false);
						}
					}
				}else if(ib==i && jb== j + taille -1){
					for(int jt = j + 1; jt < j + taille -1; jt++){
						if(this.isOccupe(i, jt)){
							grille[ib][jb].setEnabled(false);
						}
					}
				}else if(ib== i - taille +1 && jb==j){
					for(int it = i-taille + 1; it < i; it++){
						if(this.isOccupe(it, j)){
							grille[ib][jb].setEnabled(false);
						}
					}
				}else if(ib== i + taille - 1&& jb==j){
					for(int it = i+1; it < i-taille + 1; it++){
						if(this.isOccupe(it, j)){
							grille[ib][jb].setEnabled(false);
						}
					}
				}else{
					grille[ib][jb].setEnabled(false);
				}
			}
		}
		valider.setEnabled(false);
		grille[i][j].setEnabled(false);
	}

	private boolean isOccupe(int i, int j) {
		for(BateauPlacement bp : this.lbutton){
			if(bp.occupe(i,j)){
				return true;
			}
		}

		return false;
	}

	public void resetBateau() {
		bateauChoisi = null;
		place = Placement.DEBUT;

		for(int y = 0; y < grille.length; y++){
			for(int x = 0; x < grille.length; x++){			
				grille[y][x].setEnabled(false);
				grille[y][x].setIcon(ImageFactory.getInstance().getMer());
			}
		}

		boolean allBoats = true;
		for( BateauPlacement b : lbutton ) {
			b.setEnabled(true);
			if(allBoats == true && !b.isPlace()){
				allBoats = false;
			}

			if(b.isPlace()){
				int l1 = b.getiDeb();
				int c1 = b.getjDeb();
				int l2 = b.getiFin();
				int c2 = b.getjFin();

				int ld = Math.min(l1, l2);
				int cd = Math.min(c1, c2);
				int lf = Math.max(l1, l2);
				int cf = Math.max(c1, c2);

				for(int i = ld; i <= lf; i++){
					for(int j = cd; j <= cf; j++){
						grille[i][j].setIcon(null);
						grille[i][j].setEnabled(true);
						grille[i][j].setBackground(b.getBackground());
						//grille[i][j].setD//setDisableBackground(b.getBackground());
					}
				}
			}
		}

		if(allBoats){
			this.valider.setEnabled(true);
		}else{
			this.valider.setEnabled(false);
		}
	}

}


