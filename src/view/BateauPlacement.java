package view;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import model.Bateau;
import model.Element;
import model.Joueur;
import model.Partie;

public class BateauPlacement extends JButton{

	private Bateau b  ;
	private String nom ; 
	private boolean debutPlace = false;
	private int iDeb  = -1;
	private int jDeb  = -1;
	private int iFin  = -1;
	private int jFin  = -1;
	
	public BateauPlacement(Bateau b) {
		this.setPreferredSize(new Dimension(10,10));
		this.b=b ; 
		this.nom=b.nom ; 
		this.setText(nom);
	}

	public Bateau getB() {
		return b;
	}

	public void setB(Bateau b) {
		this.b = b;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isDebutPlace() {
		return debutPlace;
	}

	public void setDebutPlace(boolean debutPlace) {
		this.debutPlace = debutPlace;
	}

	public int getiDeb() {
		return iDeb;
	}

	public void setiDeb(int iDeb) {
		this.iDeb = iDeb;
	}

	public int getjDeb() {
		return jDeb;
	}

	public void setjDeb(int jDeb) {
		this.jDeb = jDeb;
	}

	public int getiFin() {
		return iFin;
	}

	public void setiFin(int iFin) {
		this.iFin = iFin;
	}

	public int getjFin() {
		return jFin;
	}

	public void setjFin(int jFin) {
		this.jFin = jFin;
	}

	public boolean occupe(int i, int j) {
		if(this.iFin != -1){
			int ideb = Math.min(this.iDeb, this.iFin);
			int ifin = Math.max(this.iDeb, this.iFin);
			int jdeb = Math.min(this.jDeb, this.jFin);
			int jfin = Math.max(this.jDeb, this.jFin);

			if(ideb != ifin){
				for(int a = ideb; a <= ifin; a++){
					if(a == i && j == jdeb){
						return true;
					}
				}
			}else{
				for(int a = jdeb; a <= jfin; a++){
					if(j ==  a && i == ideb){
						return true;
					}
				}
			}
		}
		return false;
	}

	public void reset() {
		this.iDeb = -1;
		this.iFin = -1;
		this.jDeb = -1;
		this.jFin = -1;		
	}

	public boolean isPlace() {
		if(iDeb != -1 && jDeb != -1 && iFin  != -1 && jFin != -1){
			return true;
		}
		
		return false;
	}

	public void validerPosition(Joueur joueur) throws Exception {
		this.b.setProprietaire(joueur);
		this.b.addElements(this.iDeb, this.jDeb, this.iFin, this.jFin);
	}

	public void validerPosition(Partie model) throws Exception {
		model.validerPosition(this.b, this.iDeb, this.jDeb, this.iFin, this.jFin);
	}
}
