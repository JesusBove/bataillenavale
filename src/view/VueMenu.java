package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import model.Epoque;
import model.IA;
import model.Joueur;
import model.Partie;

public class VueMenu extends JMenuBar{
	
	private Partie model ; 
	private JeuGUI jeu;
	JMenu fichier = new JMenu("Fichier") ; 
	JMenuItem save = new JMenuItem("Sauvegarder une partie");

	public VueMenu(final Partie model, JeuGUI jeuGUI) {
		this.model=model ; 
		this.jeu = jeuGUI;
		
		JMenuItem open = new JMenuItem("Charger une partie") ; 
		
		// Load listener
		open.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jeu.charger();
			}
        });
		
		
		
		// Save listener
		save.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jeu.save();
			}
        });
		
		// Nouvelle Partie
		JMenuItem nouv = new JMenuItem("Nouvelle Partie");
		nouv.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jeu.nouvelle();
			}
        });
		
		JMenuItem close = new JMenuItem("Fermer");
		close.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
        });
		
		JMenu strat = new JMenu("Changer strategie IA");
		
		JMenuItem croix = new JMenuItem("Croix");
		croix.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.getIa().setStrat(croix.getLabel());
				model.initReachable(model.getIa()) ; 
			}
        });
		
		JMenuItem alea = new JMenuItem("Aleatoire");
		alea.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.getIa().setStrat(alea.getLabel());
			}
        });
		
		strat.add(croix);
		strat.add(alea);
		fichier.add(open);
		fichier.add(save); 
		fichier.add(nouv); 
		
		fichier.add(strat);
		fichier.add(close); 
		this.add(fichier) ; 
	}

	public void disableSave() {
		this.save.setEnabled(false);
	}
}
