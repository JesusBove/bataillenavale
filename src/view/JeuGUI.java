package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.*;

public class JeuGUI extends JFrame implements Observer{

	private JFrame frame = new JFrame("Bataille Navale");
	private Partie model; 
	VueMenu vuemenu;

	public JeuGUI() {
		model = new Partie();
		model.nouvellePartie();
		buildFrame() ; 
	}

	public void buildFrame() { 
		VueSelection vueselection = new VueSelection(this.model) ; 
		model.addObserver(vueselection);
		this.vuemenu = new VueMenu(this.model, this) ; 
		vuemenu.disableSave();
		this.setJMenuBar(vuemenu); 
		this.add(vueselection,BorderLayout.WEST);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setPreferredSize(new Dimension(800,100));
		this.pack();
	}

	public void setVuePlacement(){
		this.getContentPane().removeAll();
		this.validate();
		this.repaint();
		VuePlacement vueplacement = new VuePlacement(this.model) ;	
		vuemenu.disableSave();
		this.add(vueplacement) ; 	 
		this.setPreferredSize(new Dimension(1200,600)) ;	
		this.pack();
		this.setVisible(true);
	}

	public void setVuePlateau(){		
		this.getContentPane().removeAll();
		this.validate();
		this.repaint();
		VuePlateau vueplateau = new VuePlateau(this.model) ;
		model.addObserver(vueplateau);
		model.addObserver(this);
		model.update();
		this.add(vueplateau) ; 	 
		VueMenu vuemenu = new VueMenu(this.model, this) ; 
		this.setJMenuBar(vuemenu); 
		this.setPreferredSize(new Dimension(1200,600)) ;	
		this.pack();
		this.setVisible(true);
	}

	public void charger(){    
		JFileChooser chooser = new JFileChooser() ; 
		chooser.changeToParentDirectory();
		chooser.showOpenDialog(null);
		File file = chooser.getSelectedFile() ;
		ObjectInputStream ois = null;

		
		if(file != null){
			Partie partie = null;
			Joueur j = null;
			
		    try {
			      final FileInputStream fichier = new FileInputStream(file);
			      ois = new ObjectInputStream(fichier);
			      partie = (Partie) ois.readObject();

			    } catch (final java.io.IOException e) {
			      e.printStackTrace();
			    } catch (final ClassNotFoundException e) {
			      e.printStackTrace();
			    } finally {
			      try {
			        if (ois != null) {
			          ois.close();
			        }
			      } catch (final IOException ex) {
			        ex.printStackTrace();
			      }
			    }
		    
		    this.model = partie;
		    this.setVuePlateau();
		}
	}
	
	public void save(){
		JFileChooser chooser = new JFileChooser() ; 
		chooser.changeToParentDirectory();
		chooser.showOpenDialog(null);
		File file = chooser.getSelectedFile() ;
		ObjectOutputStream oos = null;

		try {
			final FileOutputStream fichier = new FileOutputStream(file);
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(this.model);
			oos.flush();
		} catch (final java.io.IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}


	public static void main ( String[]a ) {
		new JeuGUI() ; 
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(model.isFinished()){
			this.getContentPane().removeAll();
			this.validate();
			this.repaint();	
				
			JLabel jl  ;
			JPanel stats = new JPanel();
			JLabel gagne = new JLabel();
			JPanel grilleScore = new JPanel() ; 		
			
			if(model.getGagnant() instanceof Joueur){
				jl = new JLabel(ImageFactory.getInstance().getVictoire());
				gagne.setText("Vous avez gagne Capitaine !");
				
			}else {
				jl = new JLabel(ImageFactory.getInstance().getCoule());
				gagne.setText("Vous avez lamentablement perdu Capitaine !");
			}
			
			GridLayout gl = new GridLayout(5, 3) ; 
			grilleScore.setLayout(gl);
			gl.setVgap(5);
			gl.setHgap(30);
			grilleScore.add(new JLabel(""));
			grilleScore.add(new JLabel("Joueur"));
			grilleScore.add(new JLabel("IA"));
			grilleScore.add(new JLabel("Score final"));
			grilleScore.add(new JLabel(model.getJoueur().getScore()+"")) ; 
			grilleScore.add(new JLabel(model.getIa().getScore()+"")) ; 
			grilleScore.add(new JLabel("Nombre de bateaux coules"));
			grilleScore.add(new JLabel(model.getJoueur().getNbCoules()+"")) ; 
			grilleScore.add(new JLabel(model.getIa().getNbCoules()+"")) ;
			grilleScore.add(new JLabel("Nombre de coups touches"));
			grilleScore.add(new JLabel(model.getJoueur().getNbTouche()+"")) ; 
			grilleScore.add(new JLabel(model.getIa().getNbTouche()+"")) ;
			grilleScore.add(new JLabel("Nombre de coups a la mer"));
			grilleScore.add(new JLabel(model.getJoueur().getNbLoupe()+"")) ; 
			grilleScore.add(new JLabel(model.getIa().getNbLoupe()+"")) ;
			
			GridLayout gl2 = new GridLayout(2, 0) ; 
			stats.setLayout(gl2);
			stats.add(gagne);
			stats.add(grilleScore);
			
			setLayout(new BorderLayout());
			setLayout(new FlowLayout());
			
			add(jl);
			add(stats); 
			setSize(800,600);

			this.setPreferredSize(new Dimension(800,900)) ;	
			this.pack();
			this.setVisible(true);
		}
	}

	public void nouvelle() {
		this.getContentPane().removeAll();
		this.validate();
		this.repaint();
		model = new Partie();
		model.nouvellePartie();
		buildFrame() ; 
	}
}

