package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class Case extends JButton{

	private int i,j ;
	private ImageIcon img = ImageFactory.getInstance().getMer_touche(); 
	private String text ; 
	
	public Case(int i,int j) {
		this.i=i ; 
		this.j=j ; 
		this.setMargin(new Insets(0, 0, 0, 0));
		this.setIcon(img);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}
	
}