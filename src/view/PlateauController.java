package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Partie;

public class PlateauController implements ActionListener{
	public Partie model;
	public int i,j;
	
	public PlateauController(Partie model, int i, int j) {
		this.model = model;
		this.i = i;
		this.j = j;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.model.setTouche(i,j);
		this.model.tourIA();
	}

}
