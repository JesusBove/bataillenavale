package view;

import javax.swing.ImageIcon;

public class ImageFactory { 
	private ImageIcon croix_noire = new ImageIcon(this.getClass().getResource("/croix_noire.gif"));	
	private ImageIcon croix_rouge = new ImageIcon(this.getClass().getResource("/croix_rouge.gif"));
	private ImageIcon mer = new ImageIcon(this.getClass().getResource("/mer.jpeg"));
	private ImageIcon mer_touche = new ImageIcon(this.getClass().getResource("/mer_touche.jpeg"));
	private ImageIcon check = new ImageIcon(this.getClass().getResource("/check.gif"));
	private ImageIcon coule = new ImageIcon(this.getClass().getResource("/coule.jpeg"));
	private ImageIcon victoire = new ImageIcon(this.getClass().getResource("/victoire.jpg"));

	private static ImageFactory instance = new ImageFactory() ; 
    
    private ImageFactory() { }
    
    public static ImageFactory getInstance() {
        return instance ; 
    }

	public ImageIcon getCroix_noire() {
		return croix_noire;
	}

	public ImageIcon getCroix_rouge() {
		return croix_rouge;
	}

	public ImageIcon getMer() {
		return mer;
	}

	public ImageIcon getMer_touche() {
		return mer_touche;
	}

	public ImageIcon getCheck() {
		return check;
	}

	public ImageIcon getCoule() {
		return coule;
	}

	public ImageIcon getVictoire() {
		return victoire;
	}
    
    

}
