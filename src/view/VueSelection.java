package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.EpoqueFactory;
import model.Partie;

public class VueSelection extends JPanel implements ActionListener,Observer{
	
	private Partie model ; 
	private JButton valider = new JButton("Valider") ;

	private String[] choixTailleGrille = {"5 par 5" , "10 par 10","15 par 15"} ;
	private String[] choixEpoque = {"Epoque Medievale", "Epoque Moderne","Epoque Futuriste","Epoque des Legendes"} ;
	private String[] choixStrategie = {"Aleatoire","Croix"} ;
	
	private JComboBox tailleGrille = new JComboBox(choixTailleGrille) ;
	private JComboBox epoque = new JComboBox(choixEpoque) ;
	private JComboBox strategie = new JComboBox(choixStrategie) ;
	
	public VueSelection(Partie model) {
		this.model=model ; 
		JLabel titre = new JLabel("Ecran de selection des parametres") ;
		titre.setLayout(new GridLayout(0,1));
		this.add(titre); 
			
		this.add(tailleGrille);
		this.add(epoque); 
		this.add(strategie); 
		
		
		valider.addActionListener(this);
		this.add(valider) ; 
		
	}
	
	public void actionPerformed(ActionEvent ae) {
		StringTokenizer st = new StringTokenizer((String) tailleGrille.getSelectedItem());
		int a = Integer.parseInt(st.nextToken()) ;
		model.setTailleGrille(a);
		String str = (String) strategie.getSelectedItem() ; 
		model.getIa().setStrat(str) ; 
		if (epoque.getSelectedItem().equals("Epoque Moderne")) model.setEpoque(EpoqueFactory.getInstance().nouvelleEpoque("Moderne"));
		if (epoque.getSelectedItem().equals("Epoque Futuriste")) model.setEpoque(EpoqueFactory.getInstance().nouvelleEpoque("Futuriste"));
		if (epoque.getSelectedItem().equals("Epoque Medievale")) model.setEpoque(EpoqueFactory.getInstance().nouvelleEpoque("Medievale"));
		if (epoque.getSelectedItem().equals("Epoque des Legendes")) model.setEpoque(EpoqueFactory.getInstance().nouvelleEpoque("Legende"));

		try {
			model.initBateau();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		((JeuGUI)topFrame).setVuePlacement();
		model.hasChanged() ; 
	}
	

	public JButton getValider() {
		return valider;
	}

	public void setValider(JButton valider) {
		this.valider = valider;
	}

	public JComboBox getTailleGrille() {
		return tailleGrille;
	}

	public void setTailleGrille(JComboBox tailleGrille) {
		this.tailleGrille = tailleGrille;
	}

	public JComboBox getEpoque() {
		return epoque;
	}

	public void setEpoque(JComboBox epoque) {
		this.epoque = epoque;
	}

	public JComboBox getStrategie() {
		return strategie;
	}

	public void setStrategie(JComboBox strategie) {
		this.strategie = strategie;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
	}

}
