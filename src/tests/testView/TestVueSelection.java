package testView;

import static org.junit.Assert.*;
import model.EpoqueFactory;
import model.IA;
import model.Joueur;
import model.Partie;

import org.junit.Before;
import org.junit.Test;

import view.JeuGUI;
import view.VueSelection;

public class TestVueSelection {
	
	private Partie p ; 
	private VueSelection vs ; 
	
	@Before
	public void setup() {
		p = new Partie() ; 
		p.nouvellePartie(new Joueur("Jose"), new IA("bove"), EpoqueFactory.getInstance().nouvelleEpoque("Moderne"), 15);
		vs = new VueSelection(p) ; 
	}

	@Test
	public void testAffichageComboBox() {
		String str = (String) vs.getTailleGrille().getSelectedItem() ; 
		assertEquals("5 par 5",str) ; 
	}
	
	@Test
	public void testCliqueAndRefreshTaille() {
		vs.getTailleGrille().setSelectedIndex(2);
	}
	
	@Test
	public void testCliqueAndRefreshEpoque() {
		vs.getEpoque().setSelectedIndex(1);
	}
	
	@Test
	public void testCliqueAndRefreshStrat() {
		vs.getStrategie().setSelectedIndex(1);
	}

}
