package testModel;

import static org.junit.Assert.*;
import model.IA;
import model.Joueur;

import org.junit.Before;
import org.junit.Test;

public class TestParticipant {
	
	private Joueur j ; 
	private IA ia ; 

	@Before
	public void setup() {
		j = new Joueur("Jose") ; 
		ia = new IA("Bove") ; 
		ia.initGrille(10);
	}
	
	@Test
	public void testInitGrille() {
		j.initGrille(10) ;
		assertEquals(10,j.getGrille().length) ; 
	}
	
	@Test
	public void testInitGrilleBool() {
		j.initGrille(10) ;
		assertEquals(false,j.getTouche(5,5)) ; 
	}
	
	@Test
	public void testInitPlacement() throws Exception {
		ia.initPlacementBateau(); 
	}
}