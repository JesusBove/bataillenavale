package testModel;
import static org.junit.Assert.*;
import model.Bateau;
import model.Bateau.Etat;
import model.Element;
import model.IA;
import model.Joueur;
import model.Participant;

import org.junit.Before;
import org.junit.Test;

public class TestBateau{
	
	private Bateau b ; 
	
	@Before
	public void setup() throws Exception {
		b = new Bateau("Corvette",3,2,1,0,1,2) ;
		Joueur j = new Joueur("jose") ; 
		j.initGrille(10);
		b.setProprietaire(j);
	}

	@Test
	public void testBateau() throws Exception { 
		assertTrue("Corvette".equals(b.getNom())) ; 
	}

	
	@Test(expected = Exception.class)  
	public void testAddElementsException() throws Exception {
		Bateau b2 = new Bateau("Corvette",2,2,1,1,1,4) ;	
		b2.addElements(1,0,1,2);
		fail("Taille invalide non detectee");
	}
	
	@Test
	public void testAddElements() throws Exception {
		b.addElements(1,0,1,2);
	}
	
	@Test
	public void testGetElement() throws Exception {
		b.addElements(1,0,1,2);
		Element e = b.getElement(1, 1) ; 
		assertTrue(e!=null) ; 
	}
	
	@Test
	public void testGetElementFaux() throws Exception {
		Element e = b.getElement(2, 2) ; 
		assertTrue(e==null) ; 
	}
	
	@Test
	public void testToucher() {
		b.toucher(1,1) ; 
		assertEquals(b.etat,Etat.TOUCHE) ; 
	}
	
	@Test
	public void testToucherCoule() {
		b.toucher(1,0) ; 
		b.toucher(1,1) ; 
		b.toucher(1,2) ; 
		assertEquals(b.etat,Etat.COULE) ; 
	}

	@Test
	public void testToucherIndemne() {
		assertEquals(b.etat,Etat.INDEMNE) ;
	}
	
	@Test
	public void testToucherCompteur() {
		b.toucher(1,0) ; 
		b.toucher(1,1) ; 
		assertEquals(2,b.compteurTouche) ; 
	}
	
}