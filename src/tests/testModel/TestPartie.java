package testModel;
import static org.junit.Assert.*;
import model.Bateau;
import model.Epoque;
import model.EpoqueModerne;
import model.IA;
import model.Joueur;
import model.Partie;
import model.Partie.Direction;

import org.junit.Before;
import org.junit.Test;


public class TestPartie {
	
	Partie p ; 
	Joueur j ; 
	IA ia ; 
	Epoque ep ; 
	
	@Before
	public void setup() throws Exception {
		p = new Partie() ;
		j = new Joueur("j") ; 
		ia = new IA("ia") ; 
		ep = new EpoqueModerne() ; 
		p.nouvellePartie(j, ia, ep, 10);
		p.initGrilles(10);
		p.setStratCoule(true);
		// Bateau Joueur
		Bateau b = new Bateau("Corvette",3,10,1,0,1,2) ;
		p.validerPosition(b, 1, 0, 1, 2);
		// Bateau Ia
		Bateau b3 = new Bateau("Corvette",3,10,1,0,1,2) ;
		b3.setProprietaire(ia);
		b3.addElements(1, 0, 1, 2);
	}

	@Test
	public void testNouvellePartie() { 
		p.initGrilles(10);
		assertEquals(p.getTailleGrille(),10) ; 
	}
	
	@Test
	public void testValiderPosition() throws Exception {
		assertTrue(p.getJoueur().getBateau(1,2)!=null) ; 
	}
	
	@Test
	public void testValiderPositionFaux() throws Exception {
		Bateau b2 = new Bateau("Corvette",3,2,1,0,1,2) ;
		p.validerPosition(b2, 2, 2, 2, 4);
		assertEquals(p.getJoueur().getBateau(2,5),null) ;
	}
	
	@Test
	public void testSetTouche() {
		p.setTouche(1, 1);
		boolean bool = p.getJoueur().getTouche(1, 1) ; 
		assertEquals(bool,true) ; 
	}
	
	@Test
	public void testSetToucheFaux() {
		p.setTouche(1, 1);
		boolean bool = p.getJoueur().getTouche(5,5) ; 
		assertEquals(bool,false) ; 
	}
	
	@Test
	public void testSetToucheScore() {
		p.setTouche(1, 1);
		p.setTouche(1, 2);
		assertEquals(2,p.getJoueur().getScore()) ; 
		
	}
	
	@Test
	public void testSetToucheCoule() {
		p.setTouche(1, 1);
		p.setTouche(1, 2);
		p.setTouche(1, 0);
		assertEquals(6,p.getJoueur().getScore()) ;
	}
	
	@Test
	public void testSetToucheCroix() {
		p.setToucheCroix(1, 1);
		boolean bool = p.getIa().getTouche(1, 1) ; 
		assertEquals(bool,true) ;
	}
	
	@Test
	public void testSetToucheCroixFaux() {
		p.setToucheCroix(1, 1);
		boolean bool = p.getIa().getTouche(5,5) ; 
		assertEquals(bool,false) ; 
	}
	
	@Test
	public void testSetToucheCroixScore() {
		p.setToucheCroix(1, 1);
		p.setToucheCroix(1, 2);
		assertEquals(2,p.getIa().getScore()) ; 
		
	}
	
	@Test
	public void testSetToucheCroixCoule() {
		p.setToucheCroix(1, 1);
		p.setToucheCroix(1, 2);
		p.setToucheCroix(1, 0);
		assertEquals(6,p.getIa().getScore()) ;
	}
	
	@Test
	public void testSetToucheCroixLast() {
		p.setToucheCroix(1, 1);
		p.setToucheCroix(1, 2);
		assertEquals(1,p.getLasti()) ; 
		assertEquals(2,p.getLastj()) ; 
	}
	
	@Test
	public void testSetToucheCroixFirst() {
		p.setToucheCroix(1, 1);
		p.setToucheCroix(1, 2);
		assertEquals(0,p.getFirsti()) ; 
		assertEquals(0,p.getFirstj()) ; 
	}

	@Test 
	public void testSetToucheCroixDirectionCoule() {
		p.setToucheCroix(1, 1);
		p.setToucheCroix(1, 2);
		p.setToucheCroix(1, 0);
		assertEquals(p.getLastDir().toString(),"NONE") ; 
	}
	
	@Test
	public void testStratCroixCouleFalse() {
		p.setStratCoule(false);
		p.stratCroix();
		assertEquals(p.getLastDir().toString(),"NONE") ; 
	}
	
	@Test
	public void testStratCroixCouleTrue() {
		p.stratCroix();
		assertTrue(!(p.getLastDir().toString().equals("NONE"))) ; 
	}
	
	@Test
	public void testStratCroixGaucheGauche() {
		p.setLastDir(Direction.LEFT);
		p.setLastShot(true);
		p.setLasti(5);
		p.setLastj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("LEFT")) ; 
	}
	
	@Test
	public void testStratCroixGaucheDroit() {
		p.setLastDir(Direction.LEFT);
		p.setLasti(5);
		p.setLastj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("RIGHT")) ; 
	}
	
	@Test
	public void testStratCroixGaucheBas() {
		p.setLastDir(Direction.LEFT);
		p.setFirsti(5);
		p.setFirstj(9); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("DOWN")) ; 
	}
	
	@Test
	public void testStratCroixGaucheHaut() {
		p.setLastDir(Direction.LEFT);
		p.setFirsti(9);
		p.setFirstj(9); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("UP")) ; 
	}
	
	@Test
	public void testStratCroixDroitDroit() {
		p.setLastDir(Direction.RIGHT);
		p.setLasti(5);
		p.setLastj(5); 
		p.setLastShot(true);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("RIGHT")) ;
	}
	
	@Test
	public void testStratCroixDroitGauche() {
		p.setLastDir(Direction.RIGHT);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("LEFT")) ;
	}
	
	@Test
	public void testStratCroixDroitBas() {
		p.setLastDir(Direction.RIGHT);
		p.setFirsti(5);
		p.setFirstj(0); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("DOWN")) ;
	}
	
	@Test
	public void testStratCroixDroitHaut() {
		p.setLastDir(Direction.RIGHT);
		p.setFirsti(9);
		p.setFirstj(0); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("UP")) ;
	}
	
	@Test
	public void testStratCroixBasBas() {
		p.setLastDir(Direction.DOWN);
		p.setLasti(5);
		p.setLastj(5); 
		p.setLastShot(true);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("DOWN")) ;
	}
	
	@Test
	public void testStratCroixBasHaut() {
		p.setLastDir(Direction.DOWN);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("UP")) ;
	}
	
	@Test
	public void testStratCroixBasGauche() {
		p.setLastDir(Direction.DOWN);
		p.setFirsti(0);
		p.setFirstj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("LEFT")) ;
	}
	
	@Test
	public void testStratCroixBasDroite() {
		p.setLastDir(Direction.DOWN);
		p.setFirsti(0);
		p.setFirstj(0); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("RIGHT")) ;
	}
	
	@Test
	public void testStratCroixHautHaut() {
		p.setLastDir(Direction.UP);
		p.setLasti(5);
		p.setLastj(5); 
		p.setLastShot(true);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("UP")) ;
	}
	
	@Test
	public void testStratCroixHautBas() {
		p.setLastDir(Direction.UP);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("DOWN")) ;
	}
	
	@Test
	public void testStratCroixHautGauche() {
		p.setLastDir(Direction.UP);
		p.setFirsti(9);
		p.setFirstj(5); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("LEFT")); 
	}
	
	@Test
	public void testStratCroixHautDroite() {
		p.setLastDir(Direction.UP);
		p.setFirsti(9);
		p.setFirstj(0); 
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("RIGHT")); 
	}
	
	@Test 
	public void testStratCroixGaucheDefaut() {
		p.setLastDir(Direction.LEFT);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.getIa().setTouches(4, 5);
		p.getIa().setTouches(6, 5);
		p.getIa().setTouches(5, 4);
		p.getIa().setTouches(5, 6);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("NONE"));
	}
	
	@Test 
	public void testStratCroixDroitDefaut() {
		p.setLastDir(Direction.RIGHT);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.getIa().setTouches(4, 5);
		p.getIa().setTouches(6, 5);
		p.getIa().setTouches(5, 4);
		p.getIa().setTouches(5, 6);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("NONE"));
	}
	
	@Test 
	public void testStratCroixHautDefaut() {
		p.setLastDir(Direction.UP);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.getIa().setTouches(4, 5);
		p.getIa().setTouches(6, 5);
		p.getIa().setTouches(5, 4);
		p.getIa().setTouches(5, 6);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("NONE"));
	}
	
	@Test 
	public void testStratCroixBasDefaut() {
		p.setLastDir(Direction.DOWN);
		p.setFirsti(5);
		p.setFirstj(5); 
		p.getIa().setTouches(4, 5);
		p.getIa().setTouches(6, 5);
		p.getIa().setTouches(5, 4);
		p.getIa().setTouches(5, 6);
		p.stratCroix();
		assertTrue(p.getLastDir().toString().equals("NONE"));
	}
	
}
